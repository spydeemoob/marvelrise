# MarvelRise

Memrise Coding Challenge by Kevin Malkic

# Frameworks used

- Alamofire
- Realm
- ReachabilitySwift

## Functional requirements
You are tasked with creating an application that will present a feed of characters
from the Marvel Universe. In order to gain access to the characters you will need to
create an account on https://developer.marvel.com/ this will provide you with
an API key. With this key you should be able to access the characters endpoint,
which at the time of writing is:
v1/public/characters
Details of this endpoint can be found at https://developer.marvel.com/docs.

The application should:

- Present a feed of characters as a vertical list
- For each character display:
	- Name
	- Description
	- Avatar
- Allow the user to scroll through all characters.
- Persist the character data for offline viewing.
- The UI should always be responsive.

## Technical requirements
It is enough that the application can be run on the iPhone with iOS10.
It should be written in Swift 3.