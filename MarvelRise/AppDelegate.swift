//
//  AppDelegate.swift
//  MarvelRise
//
//  Created by Kevin Malkic on 06/05/2017.
//  Copyright © 2017 Kevin Malkic. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let viewController = CharacterListViewController()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = self.viewController
        self.window?.makeKeyAndVisible()
        
        return true
    }
}

