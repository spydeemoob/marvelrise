//
//  CharactersControllerProtocol.swift
//  MarvelRise
//
//  Created by Kevin Malkic on 08/05/2017.
//  Copyright © 2017 Kevin Malkic. All rights reserved.
//

import Foundation
import UIKit

protocol CharactersControllerProtocol {
    
    typealias ImageHandler = (UIImage) -> Void
    
    func searchForName(name: String?)
    func getDataSourceCount() -> Int
    func hasMoreToLoad(lastIndex: UInt) -> Bool
    func fetchNextList()
    func didReachLimit() -> Bool
    func getCharacter(index: Int) -> Character?
    func getCharacterAvatar(index: Int, imageHandler: @escaping ImageHandler)
}
