//
//  CharacterRestController.swift
//  MarvelRise
//
//  Created by Kevin Malkic on 06/05/2017.
//  Copyright © 2017 Kevin Malkic. All rights reserved.
//

import Foundation
import Alamofire

class CharactersRestController: CharactersControllerProtocol {
    
    weak var fetchDelegate: CharactersFetchData?
    
    private var offset: UInt = 0
    private var loading: Bool {
        return self.request != nil
    }
    private var limit: UInt = 50
    private var total: UInt = 0
    private var limitReached: Bool {
        return total > 0 && offset >= total
    }
    private var characters = [Character]()
    private var nameSearch: String?
    private var request: DataRequest?
    private var shouldReload: Bool {
        return self.offset == 0
    }
    
    var dataDidUpdate: ((Int, Int, Bool) -> Void)?
    var errorHandler: ((String) -> Void)?
    
    private let destination: DownloadRequest.DownloadFileDestination = { _, response in
        
        return (CharactersControllerHandler.destinationPath(filename: response.url?.lastPathComponent ?? "image.jpg"), [.removePreviousFile, .createIntermediateDirectories])
    }
        
    //MARK: Public
    
    func didReachLimit() -> Bool {
    
        return self.limitReached
    }
    
    func searchForName(name: String?) {
        
        if self.nameSearch == name || (self.nameSearch == nil && name?.isEmpty == true) {
            
            return
        }
        
        self.request?.cancel()
        self.request = nil
        
        self.nameSearch = name?.isEmpty == true ? nil : name
        self.total = 0
        self.offset = 0
        
        self.fetchNextList()
    }
    
    func getDataSourceCount() -> Int {
        
        return characters.count
    }
    
    func hasMoreToLoad(lastIndex: UInt) -> Bool {
        
        if self.limitReached {
            
            return false
        }
        
        return (self.offset > 0 && lastIndex >= self.offset - (self.limit / 2))
    }
    
    func fetchNextList() {
        
        if self.loading {
            
            return
        }
        
        self.request = MarvelAPI.characters(offset: self.offset, limit: self.limit, name: self.nameSearch).request { [weak self] (response: DataResponse<CharactersPackage>) in
            
            guard let charactersPackage = response.result.value, let shouldReload = self?.shouldReload, response.result.isSuccess else {
                
                if let error = response.result.error as? BackendError {
                    
                    switch error {
                        
                    case .cancelled:
                        return
                        
                    default:
                        self?.errorHandler?(error.localizedDescription)
                    }
                }

                self?.request = nil
                
                return
            }
            
            self?.total = UInt(charactersPackage.total)
            
            if var storedCharacters = self?.characters, !shouldReload {
                
                storedCharacters += charactersPackage.characters
                
                self?.characters = storedCharacters
                
            } else if shouldReload {
                
                self?.characters = Array(charactersPackage.characters)
            }
            
            if let from = self?.offset, let characters = self?.characters {
                
                DispatchQueue.main.async(execute: {
                    
                    self?.dataDidUpdate?(Int(from), Int(from + UInt(charactersPackage.count)), shouldReload)
                    self?.fetchDelegate?.charactersReceived(characters: characters)
                })
            }
            
            self?.offset = UInt(self?.characters.count ?? 0)
            
            self?.request = nil
        }
    }
    
    func getCharacter(index: Int) -> Character? {
        
        return self.characters[index]
    }
    
    func getCharacterAvatar(index: Int, imageHandler: @escaping ImageHandler) {
        
        if let character = getCharacter(index: index), let url = URL(string: character.avatarUrl) {
            
            if !CharactersControllerHandler.fileExists(filename: url.lastPathComponent) {
                
                Alamofire.download(character.avatarUrl, to: self.destination).responseData(queue: DispatchQueue.global(qos: .background), completionHandler: { response in
                    
                    if let data = response.result.value, let image = UIImage(data: data) {
                        
                        if let cropImage = image.scaleImage(toSize: CGSize(width: 100, height: 100)), let imageData = UIImageJPEGRepresentation(cropImage, 10) {
                            
                            try! imageData.write(to: CharactersControllerHandler.destinationPath(filename: url.lastPathComponent))
                            
                            DispatchQueue.main.async(execute: {
                                
                                imageHandler(cropImage)
                            })
                            
                        } else {
                            
                            DispatchQueue.main.async(execute: {
                                
                                imageHandler(image)
                            })
                        }
                    }
                })
                
            } else {
                
                let destination = CharactersControllerHandler.destinationPath(filename: url.lastPathComponent)
                
                DispatchQueue.global(qos: .background).async {
                    
                    if let data = try? Data(contentsOf: destination), let image = UIImage(data: data) {
                        
                        DispatchQueue.main.async(execute: {
                            
                            imageHandler(image)
                        })
                    }
                }
            }
        }
    }
}

