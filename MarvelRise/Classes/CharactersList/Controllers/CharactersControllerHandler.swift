//
//  CharactersControllerHandler.swift
//  MarvelRise
//
//  Created by Kevin Malkic on 08/05/2017.
//  Copyright © 2017 Kevin Malkic. All rights reserved.
//

import Foundation
import ReachabilitySwift

protocol CharactersControllerHandlerDelegate: NSObjectProtocol {
    
    func controllerDidChange()
    func errorReceived(errorMessage: String)
    func dataDidUpdate(from: Int, to: Int, shouldReload: Bool)
}

class CharactersControllerHandler {
    
    public var controller: CharactersControllerProtocol?
    weak public var delegate: CharactersControllerHandlerDelegate? {
        
        didSet {
            
            do {
                try self.reachability.startNotifier()
                
            } catch {
                
            }
        }
    }
    
    private let reachability = Reachability()!
    private let realmController = CharactersRealmController()
    
    init() {

        self.reachability.whenReachable = { [weak self] reachability in
            
            print("Reachable")
            
            DispatchQueue.main.async {
                
                let restController = CharactersRestController()
                
                restController.errorHandler = { [weak self] (errorMessage) in
                    
                    self?.delegate?.errorReceived(errorMessage: errorMessage)
                }
                
                restController.dataDidUpdate = { [weak self] (from, to, shouldReload) in
                    
                    self?.delegate?.dataDidUpdate(from: from, to: to, shouldReload: shouldReload)
                }
                
                restController.fetchDelegate = self?.realmController
                
                self?.controller = restController
                
                self?.delegate?.controllerDidChange()
            }
        }
        
        self.reachability.whenUnreachable = { [weak self] reachability in
            
            print("UnReachable")
            
            DispatchQueue.main.async {
                
                self?.controller = self?.realmController
                
                self?.delegate?.controllerDidChange()
            }
        }
    }
    
    deinit {
        
        self.reachability.stopNotifier()
    }
    
    static func destinationPath(filename: String) -> URL {
        
        let documentsPath = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)[0]
        let documentsURL = URL(fileURLWithPath: documentsPath, isDirectory: true)
        let fileURL = documentsURL.appendingPathComponent(filename)
        
        return fileURL
    }
    
    static func fileExists(filename: String) -> Bool {
        
        return FileManager.default.fileExists(atPath: CharactersControllerHandler.destinationPath(filename: filename).path)
    }
}
