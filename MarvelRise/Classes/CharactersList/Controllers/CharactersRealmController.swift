//
//  CharactersRealmController.swift
//  MarvelRise
//
//  Created by Kevin Malkic on 08/05/2017.
//  Copyright © 2017 Kevin Malkic. All rights reserved.
//

import Foundation
import RealmSwift

protocol CharactersFetchData: NSObjectProtocol {
    
    func charactersReceived(characters: [Character])
}

class CharactersRealmController: NSObject, CharactersControllerProtocol {
        
    let realm: Realm
    
    override init() {
        
        self.realm = try! Realm()
    }
    
    func didReachLimit() -> Bool {
        
        return true
    }
    
    func searchForName(name: String?) {
        
        //Todo
    }
    
    func getDataSourceCount() -> Int {
        
        return self.realm.objects(Character.self).count
    }
    
    func hasMoreToLoad(lastIndex: UInt) -> Bool {
        
        return false
    }
    
    func fetchNextList() {

    }
    
    func getCharacter(index: Int) -> Character? {
        
        return self.realm.objects(Character.self)[index]
    }
    
    func getCharacterAvatar(index: Int, imageHandler: @escaping ImageHandler) {
        
        if let character = self.getCharacter(index: index), let url = URL(string: character.avatarUrl) {
            
            if CharactersControllerHandler.fileExists(filename: url.lastPathComponent) {
                
                let destination = CharactersControllerHandler.destinationPath(filename: url.lastPathComponent)
                
                DispatchQueue.global(qos: .background).async {
                    
                    if let data = try? Data(contentsOf: destination), let image = UIImage(data: data) {
                        
                        DispatchQueue.main.async(execute: {
                            
                            imageHandler(image)
                        })
                    }
                }
            }
        }
    }
}

extension CharactersRealmController: CharactersFetchData {

    func charactersReceived(characters: [Character]) {
     
        self.realm.beginWrite()
        
        characters.forEach { (character) in
            
            self.realm.add(character, update: true)
        }
        
        try! self.realm.commitWrite()
    }
}
