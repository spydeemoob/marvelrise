//
//  Character.swift
//  MarvelRise
//
//  Created by Kevin Malkic on 06/05/2017.
//  Copyright © 2017 Kevin Malkic. All rights reserved.
//

import Foundation
import Alamofire
import RealmSwift

class Character: Object {
    
    dynamic var id: Int = 0
    dynamic var name: String = ""
    dynamic var descr: String = ""
    dynamic var avatarUrl: String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

extension Character: ResponseObjectSerializable {

    func populate(response: HTTPURLResponse, representation: Any) {
        
        guard
            let representation = representation as? [String: Any],
            let id = representation["id"] as? Int,
            let name = representation["name"] as? String,
            let description = representation["description"] as? String,
            let thumbnail = representation["thumbnail"] as? [String: String],
            let imagePath = thumbnail["path"],
            let imageExt = thumbnail["extension"]
            else {
                return
        }
        
        self.id = id
        self.name = name
        self.descr = description
        self.avatarUrl = imagePath + "." + imageExt
    }
}
