//
//  Character+ResponseCollectionSerializable.swift
//  MarvelRise
//
//  Created by Kevin Malkic on 06/05/2017.
//  Copyright © 2017 Kevin Malkic. All rights reserved.
//

import Foundation

extension Character {
    
    static func collection(from response: HTTPURLResponse, withRepresentation representation: Any) -> [Character] {
        
        var collection: [Character] = []
        
        guard
            let JSON = representation as? [String: Any],
            let data = JSON["data"] as? [String: Any],
            let results = data["results"] as? [Any]
            else {
                return collection
        }
        
        for itemRepresentation in results {
            
            let item = Character()
            item.populate(response: response, representation: itemRepresentation)
            collection.append(item)
        }
        
        return collection
    }
}
