//
//  CharactersPackage.swift
//  MarvelRise
//
//  Created by Kevin Malkic on 07/05/2017.
//  Copyright © 2017 Kevin Malkic. All rights reserved.
//

import Foundation
import RealmSwift

class CharactersPackage: Object, ResponseObjectSerializable {
    
    var total: Int = 0
    var count: Int = 0
    var characters = List<Character>()
    
    func populate(response: HTTPURLResponse, representation: Any) {
        
        guard
            let JSON = representation as? [String: Any],
            let data = JSON["data"] as? [String: Any],
            let total = data["total"] as? Int,
            let count = data["count"] as? Int
            else {
                return
        }
        
        self.total = total
        self.count = count
        self.characters = List<Character>(Character.collection(from: response, withRepresentation: representation))
    }
}
