//
//  CharacterListViewController.swift
//  MarvelRise
//
//  Created by Kevin Malkic on 06/05/2017.
//  Copyright © 2017 Kevin Malkic. All rights reserved.
//

import Foundation
import UIKit

class CharacterListViewController: UIViewController {
    
    let loadingCellIdentifier = "loading"
    let characterCellIdentifier = "cell"
    
    let controllerHandler = CharactersControllerHandler()
    let searchController = UISearchController(searchResultsController: nil)
    
    override func loadView() {
        
        self.view = CharacterListView(frame: .zero)
    }
    
    var characterListView: CharacterListView {
        
        return self.view as! CharacterListView
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.setupTableView()
        self.setupSearchController()
        self.setupController()
    }
    
    private func setupTableView() {
        
        self.characterListView.tableView.contentInset.top = UIApplication.shared.statusBarFrame.height
        self.characterListView.tableView.register(CharacterListCell.self, forCellReuseIdentifier: characterCellIdentifier)
        self.characterListView.tableView.register(LoadingCell.self, forCellReuseIdentifier: loadingCellIdentifier)
        self.characterListView.tableView.delegate = self
        self.characterListView.tableView.dataSource = self
        self.characterListView.tableView.tableHeaderView = self.searchController.searchBar
    }
    
    private func setupSearchController() {
        
        self.definesPresentationContext = true
        self.searchController.searchResultsUpdater = self
        self.searchController.dimsBackgroundDuringPresentation = false
    }
    
    private func setupController() {
        
        self.controllerHandler.delegate = self
    }
    
    fileprivate func isLastRow(indexPath: IndexPath) -> Bool {
        
        return indexPath.row == self.characterListView.tableView.numberOfRows(inSection: 0) - 1
    }
}

extension CharacterListViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let controller = self.controllerHandler.controller else {
            
            return 0
        }
        
        return controller.getDataSourceCount() + (controller.didReachLimit() ? 0 : 1)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let controller = self.controllerHandler.controller else {
            
            return CharacterListCell(style: .default, reuseIdentifier: self.characterCellIdentifier)
        }
        
        let identifier = self.isLastRow(indexPath: indexPath) && !controller.didReachLimit() ? self.loadingCellIdentifier : self.characterCellIdentifier
        
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        
        if let cell = cell as? CharacterListCell {
            
            if let character = controller.getCharacter(index: indexPath.row) {
                
                cell.updateTitle(title: character.name)
                cell.updateDescription(description: character.descr.isEmpty ? "No description" : character.descr)
            }
        }
        
        return cell
    }
}

extension CharacterListViewController: CharactersControllerHandlerDelegate {

    func controllerDidChange() {
        
        self.characterListView.tableView.reloadData()
        
        self.controllerHandler.controller?.fetchNextList()
    }
    
    func errorReceived(errorMessage: String) {
        
         print(errorMessage)
    }
    
    func dataDidUpdate(from: Int, to: Int, shouldReload: Bool) {
        
        if let count = self.controllerHandler.controller?.getDataSourceCount() {
            
            let tableView = self.characterListView.tableView
            
            if shouldReload {
                
                if count > 0 {
                    
                    tableView.setContentOffset(CGPoint(x: 0, y: -tableView.contentInset.top), animated: false)
                }
                
                tableView.reloadData()
                
            } else {
                
                tableView.beginUpdates()
                
                if to - from > 0 {
                    
                    let indexPaths = (from..<to).map { IndexPath(row: $0, section: 0) }
                    tableView.insertRows(at: indexPaths, with: .automatic)
                }
                
                if self.controllerHandler.controller?.didReachLimit() == true {
                    
                    tableView.deleteRows(at: [IndexPath(row: tableView.numberOfRows(inSection: 0) - 1, section: 0)], with: .automatic)
                }
                
                tableView.endUpdates()
            }
        }
    }
}

extension CharacterListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 88
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if let cell = cell as? CharacterListCell {
            
            self.controllerHandler.controller?.getCharacterAvatar(index: indexPath.row, imageHandler: { [weak cell, weak tableView] (image) in
                
                if let cell = cell {
                    
                    if tableView?.indexPath(for: cell)?.row == indexPath.row {
                        
                        cell.updateImage(image: image)
                    }
                }
            })
            
            if self.controllerHandler.controller?.hasMoreToLoad(lastIndex: UInt(indexPath.row)) == true {
                
                self.controllerHandler.controller?.fetchNextList()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension CharacterListViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        
        self.controllerHandler.controller?.searchForName(name: searchController.searchBar.text)
    }
}
