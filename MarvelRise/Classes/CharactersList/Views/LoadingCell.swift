//
//  LoadingCell.swift
//  MarvelRise
//
//  Created by Kevin Malkic on 07/05/2017.
//  Copyright © 2017 Kevin Malkic. All rights reserved.
//

import Foundation
import UIKit

class LoadingCell: UITableViewCell {
    
    let activity = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
    let label = UILabel()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.contentView.addSubview(activity)
        self.contentView.addSubview(label)
        
        self.setup()
        self.setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        
        self.activity.translatesAutoresizingMaskIntoConstraints = false
        self.label.translatesAutoresizingMaskIntoConstraints = false
        
        self.activity.startAnimating()
        self.label.text = "Loading next characters ..."
        self.label.textColor = .white
        self.label.font = UIFont(name: "DroidSans-Bold", size: 14)
        
        self.contentView.backgroundColor = .black
    }
    
    override func prepareForReuse() {
        
        super.prepareForReuse()
        
        self.activity.startAnimating()
    }
    
    private func setupConstraints() {
        
        let views = ["activity": self.activity, "label": self.label]
        
        var allConstraints = [NSLayoutConstraint]()
        
        allConstraints += NSLayoutConstraint.constraints(withVisualFormat: "H:|-[activity]-[label]-15-|", options: [], metrics: nil, views: views)
        allConstraints += NSLayoutConstraint.constraints(withVisualFormat: "V:|[activity]|", options: [], metrics: nil, views: views)
        allConstraints += NSLayoutConstraint.constraints(withVisualFormat: "V:|-[label]-|", options: [], metrics: nil, views: views)
        
        NSLayoutConstraint.activate(allConstraints)
    }
}
