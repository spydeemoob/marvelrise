//
//  CharacterListView.swift
//  MarvelRise
//
//  Created by Kevin Malkic on 06/05/2017.
//  Copyright © 2017 Kevin Malkic. All rights reserved.
//

import UIKit

class CharacterListView: UIView {
    
    let tableView = UITableView(frame: .zero, style: .plain)
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        self.addSubview(tableView)
    
        self.setup()
        self.setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setup() {
        
        self.tableView.translatesAutoresizingMaskIntoConstraints = false
    }
    
    private func setupConstraints() {
        
        let views = ["tableView": self.tableView]
        
        var allConstraints = [NSLayoutConstraint]()
        
        allConstraints += NSLayoutConstraint.constraints(withVisualFormat: "H:|[tableView]|", options: [], metrics: nil, views: views)
        allConstraints += NSLayoutConstraint.constraints(withVisualFormat: "V:|[tableView]|", options: [], metrics: nil, views: views)
        
        NSLayoutConstraint.activate(allConstraints)
    }
}
