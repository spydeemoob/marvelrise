//
//  CharacterListCell.swift
//  MarvelRise
//
//  Created by Kevin Malkic on 06/05/2017.
//  Copyright © 2017 Kevin Malkic. All rights reserved.
//

import Foundation
import UIKit

class CharacterListCell: UITableViewCell {
    
    let avatarImageView = UIImageView()
    let label = UILabel()
    let descriptionLabel = UILabel()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.contentView.addSubview(avatarImageView)
        self.contentView.addSubview(label)
        self.contentView.addSubview(descriptionLabel)
        
        self.setup()
        self.setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        
        self.avatarImageView.translatesAutoresizingMaskIntoConstraints = false
        self.label.translatesAutoresizingMaskIntoConstraints = false
        self.descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        
        self.avatarImageView.clipsToBounds = true
        self.avatarImageView.contentMode = .scaleAspectFill

        self.label.font = UIFont(name: "DroidSans-Bold", size: 14)
        self.descriptionLabel.font = UIFont(name: "DroidSans", size: 11)
        
        self.descriptionLabel.numberOfLines = 0
    }
    
    private func setupConstraints() {
        
        let views = ["avatar": self.avatarImageView, "label": self.label, "description": self.descriptionLabel]
        
        var allConstraints = [NSLayoutConstraint]()
        
        allConstraints += NSLayoutConstraint.constraints(withVisualFormat: "H:|[avatar(88)]-[label]-15-|", options: [], metrics: nil, views: views)
        allConstraints += NSLayoutConstraint.constraints(withVisualFormat: "H:[description(==label)]-15-|", options: [], metrics: nil, views: views)
        allConstraints += NSLayoutConstraint.constraints(withVisualFormat: "V:|[avatar]|", options: [], metrics: nil, views: views)
        allConstraints += NSLayoutConstraint.constraints(withVisualFormat: "V:|-[label(16)]-[description]-|", options: [], metrics: nil, views: views)
        
        NSLayoutConstraint.activate(allConstraints)
    }
    
    override func prepareForReuse() {
        
        super.prepareForReuse()
        
        self.avatarImageView.image = nil
        self.label.text = nil
    }
}

extension CharacterListCell {
    
    func updateImage(image: UIImage) {
        
        if self.avatarImageView.image != image {
            
            self.avatarImageView.alpha = 0
            
            self.avatarImageView.image = image
            
            UIView.animate(withDuration: 0.5) { [weak self] () in
                
                self?.avatarImageView.alpha = 1
            }
        }
    }
    
    func updateTitle(title: String) {
    
        self.label.text = title
    }
    
    func updateDescription(description: String) {
    
        self.descriptionLabel.text = description
    }
}

