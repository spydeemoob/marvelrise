//
//  MarvelAPI.swift
//  MarvelRise
//
//  Created by Kevin Malkic on 06/05/2017.
//  Copyright © 2017 Kevin Malkic. All rights reserved.
//

import Foundation
import Alamofire
import RealmSwift

enum MarvelAPI {
    
    case characters(offset:UInt, limit: UInt, name: String?)
    case character(id: String)
}

extension MarvelAPI {
    
    private static let publicKey = "187f2a177595c5c260b0e0e3823c9e69"
    private static let hash = "a34358ab7d15c3260c468b46f1ee1ffb"
    
    private static let endPoint: String = "https://gateway.marvel.com"
    private static var parameters: [String: String] {
        
        return ["apikey": publicKey, "hash": hash, "ts": "1"]
    }
    
    var path: String {
        
        switch self {
            
        case .characters:
            return MarvelAPI.endPoint + "/v1/public/characters"
            
        case let .character(id):
            return MarvelAPI.endPoint + "/v1/public/characters/\(id)"
            
        }
    }
    
    var parameters: [String: Any]? {
        
        var param = MarvelAPI.parameters
        
        switch self {
        case let .characters(offset, limit, name):
            param.updateValue(String(offset), forKey: "offset")
            param.updateValue(String(limit), forKey: "limit")
            if let name = name {
                param.updateValue(name, forKey: "nameStartsWith")
            }
        case .character:
            break
        }
        
        return param
    }
    
    func request<T: Object>(completionHandler: @escaping (DataResponse<T>) -> Void) -> DataRequest? where T : ResponseObjectSerializable {
                
        switch self {
            
        case .characters:
            return Alamofire.request(self.path, parameters: self.parameters).responseObject(completionHandler: completionHandler)
            
        case .character:
            break
        }
        
        return nil
    }
}
