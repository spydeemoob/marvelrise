//
//  BackendError.swift
//  MarvelRise
//
//  Created by Kevin Malkic on 06/05/2017.
//  Copyright © 2017 Kevin Malkic. All rights reserved.
//

import Foundation

enum BackendError: Error {
    
    case cancelled
    case network(error: Error) // Capture any underlying Error from the URLSession API
    case dataSerialization(error: Error)
    case jsonSerialization(error: Error)
    case objectSerialization(reason: String)
}
