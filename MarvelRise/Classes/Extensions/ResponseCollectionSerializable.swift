//
//  ResponseCollectionSerializable.swift
//  MarvelRise
//
//  Created by Kevin Malkic on 06/05/2017.
//  Copyright © 2017 Kevin Malkic. All rights reserved.
//

import Foundation
import Alamofire

protocol ResponseCollectionSerializable {
    
    static func collection(from response: HTTPURLResponse, withRepresentation representation: Any) -> [Self]
}

extension DataRequest {
    
    @discardableResult
    func responseCollection<T: ResponseCollectionSerializable>(
        queue: DispatchQueue? = nil,
        completionHandler: @escaping (DataResponse<[T]>) -> Void) -> Self
    {
        let responseSerializer = DataResponseSerializer<[T]> { request, response, data, error in
            
            guard error == nil else {
                
                if let error = error as NSError? {
                    
                    if error.code == -999 {
                        
                        return .failure(BackendError.cancelled)
                    }
                }
                
                return .failure(BackendError.network(error: error!))
            }
            
            let jsonSerializer = DataRequest.jsonResponseSerializer(options: .allowFragments)
            let result = jsonSerializer.serializeResponse(request, response, data, nil)
            
            guard case let .success(jsonObject) = result else {
                return .failure(BackendError.jsonSerialization(error: result.error!))
            }
            
            guard let response = response else {
                let reason = "Response collection could not be serialized due to nil response."
                return .failure(BackendError.objectSerialization(reason: reason))
            }
            
            return .success(T.collection(from: response, withRepresentation: jsonObject))
        }
        
        return response(responseSerializer: responseSerializer, completionHandler: completionHandler)
    }
}
