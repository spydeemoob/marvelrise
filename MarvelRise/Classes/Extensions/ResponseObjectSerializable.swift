//
//  ResponseObjectSerializable.swift
//  MarvelRise
//
//  Created by Kevin Malkic on 06/05/2017.
//  Copyright © 2017 Kevin Malkic. All rights reserved.
//

import Foundation
import Alamofire
import RealmSwift

protocol ResponseObjectSerializable {
    
    func populate(response: HTTPURLResponse, representation: Any)
}

extension DataRequest {
    
    func responseObject<T: Object>(
        queue: DispatchQueue? = nil,
        completionHandler: @escaping (DataResponse<T>) -> Void)
        -> Self where T : ResponseObjectSerializable
    {
        let responseSerializer = DataResponseSerializer<T> { request, response, data, error in
            
            guard error == nil else {
                
                if let error = error as NSError? {
                    
                    if error.code == -999 {
                        
                        return .failure(BackendError.cancelled)
                    }
                }
                
                return .failure(BackendError.network(error: error!))
            }
            
            let jsonResponseSerializer = DataRequest.jsonResponseSerializer(options: .allowFragments)
            let result = jsonResponseSerializer.serializeResponse(request, response, data, nil)
            
            guard case let .success(jsonObject) = result else {
                return .failure(BackendError.jsonSerialization(error: result.error!))
            }
            
            guard let response = response else {
                return .failure(BackendError.objectSerialization(reason: "JSON could not be serialized: \(jsonObject)"))
            }
            
            let responseObject = T()
            responseObject.populate(response: response, representation: jsonObject)
            
            return .success(responseObject)
        }
        
        return response(queue: queue, responseSerializer: responseSerializer, completionHandler: completionHandler)
    }
}
